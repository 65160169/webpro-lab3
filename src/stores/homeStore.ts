import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const usehomeStore = defineStore('home', () => {
  const num = ref(0)
  function doinc() {
    num.value++
  }

  return { num, doinc }
})
