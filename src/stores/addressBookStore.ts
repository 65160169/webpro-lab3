import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('AddressBook', () => {

const address = ref<AddressBook>({
  id: 0,
  name: '',
  tel: '',
  gender: 'Male'
})

let lastId = 1

const addressList = ref<AddressBook[]>([])
const isAddnew = ref(false)
function save() {
  if (address.value.id > 0) {
    //edit
    const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
    addressList.value[editedIndex] = address.value
  } else {
    addressList.value.push({ ...address.value, id: lastId++ })
  }
  isAddnew.value = false;
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

function edit(id: number) {
  isAddnew.value = true;
  const editedIndex = addressList.value.findIndex((item) => item.id === id)
  address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex]))
  //copy object in typescript
}

function remove(id: number) {
  const removeIndex = addressList.value.findIndex((item) => item.id === id)
  addressList.value.splice(removeIndex, 1)
  //copy object in typescript
}

function cancel(){
  isAddnew.value = false;
  address.value = {
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  }
}

  return { address, addressList, save, isAddnew, edit, remove, cancel }
})
